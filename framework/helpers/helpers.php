<?php
//namespace App\framework\helpers;
/**
* Helps
*/


if (! function_exists('json')) {

    function json($data = null)
    {
    	echo "<pre>";
        echo json_encode($data);
        echo "</pre>";
		exit;
    }
}

 /**
 *	Die and dump helper
 */
 if(! function_exists("dd")){
 	function dd($data){

 		echo "<pre>";
 			var_dump($data);
 		echo "</pre>";
 		exit;
 	}
 }

 /*
 *	Return the view and data
*/
if(! function_exists("view")){
    function view($view, $data = []){
        extract($data);
        require dirname(ROOT_DIR)."/app/views/{$view}.view.php";
    }
}

 /**
 *	Return data encoded to json
 */
 if(! function_exists("json")){
 	function json($data){
 		echo json_encode($data);
 		exit;
 	}
 }


 if (! function_exists("header")){
     function header(){
         require_once "";
     }

 }

if (! function_exists("footer")){
    function footer(){
        require_once "";
    }

}

