<?php
namespace App\Core;
/**
 * Created by PhpStorm.
 * User: jacquesuwamungu
 * Date: 07/05/2017
 * Time: 04:00
 */

class App
{
    protected static $registry = [];


    /**
     * @param $key
     * @param $value
     */
    public static function bind($key, $value){
        static::$registry[$key] = $value;
    }

    /**
     * @param $key
     * @return mixed
     * @throws Exception
     */
    public static function get($key){
        if (array_key_exists($key, static::$registry)){
            return static::$registry[$key];
        }
       throw new \Exception("No {$key} found in the container");
    }


}