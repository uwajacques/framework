<?php
namespace App\Core\Database;

/**
* 
*/
class QueryBuilder
{
	protected $pdo;
	
	public function __construct(\PDO $pdo){
		$this->pdo = $pdo;
	}
	

	public function all($table)
	{
		$statement = $this->pdo->prepare("select * from {$table}");
		$statement->execute();
		return $statement->fetchAll(\PDO::FETCH_CLASS);
	}

	public function one(){

    }

    public function first(){

    }


    public function last(){

    }

    /**
     * @param $table
     * @param $parms
     */
    public function create($table, $params){

        $sql = sprintf(
            'insert into %s (%s) values (%s)',
            $table,
            implode(', ', array_keys($params)),
            ':' . implode(', :', array_keys($params))
        );

        try{
            $statement = $this->pdo->prepare($sql);
            $statement->execute($params);
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }


    }

}