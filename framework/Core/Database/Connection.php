<?php
namespace App\Core\Database;
/**
* DB Connection 
*/
class Connection 
{
	public  static  function connect($config)
	{
		try{
			return new \PDO(
				$config["driver"].":host=".$config["host"].';dbname='.$config["Database"],
				$config["username"],
				$config["password"],
				$config["options"]
			);
		}catch( \PDOException $e){
			die($e->getMessage());
		}
	}
}