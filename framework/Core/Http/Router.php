<?php
namespace App\Core\Http;
/**
* 
*/

class Router
{
    /**
     *  Allowed request methods
     */
	protected  $routes = [
	    "GET" => [],
        "POST" => [],
        "DELETE" => [],
        "PUT" => [],
        "PATCH" => [],
    ];


    /**
     *  Load the file
     */
    public static function load($file){
        $router = new self;
        require $file;
        return $router;
    }


    /**
     * @param $uri
     * @param $controller
     */
    public function get($uri, $controller){
        $this->routes["GET"][$uri] = $controller;
    }


    /**
     * @param $uri
     * @param $controller
     */
    public function post($uri, $controller)
    {
        $this->routes["POST"][$uri] = $controller;
    }


    /**
     * @param $uri
     * @param $controller
     */
    public function delete($uri, $controller){
        $this->routes["DELETE"][$uri] = $controller;
    }


    /**
     * @param $uri
     * @param $controller
     */
    public function put($uri, $controller){
        $this->routes["PUT"][$uri] = $controller;
    }


    /**
     * @param $uri
     * @param $controller
     */
    public function patch($uri, $controller){
        $this->routes["PATCH"][$uri] = $controller;
    }


    /**
     * @param $uri
     * @return mixed
     * @throws Exception
     */
    public function direct($uri, $methodType){

		if (array_key_exists($uri, $this->routes[$methodType])){
		    return $this->callControllerMethod(
                ...explode("@", $this->routes[$methodType][$uri])
            );
		}
        throw new \Exception('No route defined for this URI.');
	}


    /**
     * @param $controller
     * @param $method
     * @return mixed
     * @throws Exception
     */
    protected function callControllerMethod($controller, $method){
        $controller = "App\Controllers\\{$controller}";
        $controller = new $controller;
        if (! method_exists($controller, $method)){
            throw new \Exception(
                "{$controller} does not have this method: {$method}."
            );
        }
        return $controller->$method();
    }
}