<?php

$router->get("",  "MainCtrl@index");
$router->get("about",  "MainCtrl@about");
$router->get("contact",  "MainCtrl@contact");
$router->get("signup",  "MainCtrl@getsignup");
$router->post("signup",  "MainCtrl@postsignup");

