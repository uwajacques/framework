<?php

require 'vendor/autoload.php';
require 'framework/bootstrap/bootstrap.php';
use App\Core\Http\Router;
use App\Core\Http\Request;

Router::load('app/routes/routes.php')
    ->direct(Request::uri(), Request::method());


